﻿using Klijent.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Klijent.Controllers
{
    public class HomeController : Controller
    {
        

        public IActionResult Hrana()
        {
            return View();
        }

        public IActionResult Pice()
        {
            return View();
        }

        public IActionResult Naruci()
        {
            return View();
        }

        public IActionResult NaruciGost()
        {
            return View();
        }


    }
}
