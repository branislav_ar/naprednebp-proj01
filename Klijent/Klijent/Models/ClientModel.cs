﻿namespace Klijent.Models
{
    public class ClientModel
    {
        public const string connectionURL = "ws:////localhost:8600";

        public string ClientId { get; set; }

        public string Message { get; set; }

        public int CommandKey { get; set; }
    }
}
