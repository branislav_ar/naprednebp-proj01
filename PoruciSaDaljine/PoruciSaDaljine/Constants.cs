﻿using System;

namespace PoruciSaDaljine
{
    public class Constants
    {
        public const string REDIS_CONNECTION = "localhost:6379";
        public const string SERVER_URL = "http://localhost:8600";
    }
}
