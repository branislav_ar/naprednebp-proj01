﻿namespace PoruciSaDaljine.Models
{
    public class WSMessage
    {
        public enum COMMAND_KEY
        {
            CLIENT_SUBSCRIBE = 0,
            CLIENT_UNSUBSCRIBE = 1,
            CLIENT_MESSAGE = 2,
            CLIENT_GET_CHANNELS = 3,
            SERVER_RESULT = 100
        }

        public COMMAND_KEY CommandKey { get; set; }
        public string Payload { get; set; }

    }
}
