﻿namespace PoruciSaDaljine.Models
{
    public class ControlMessage
    {
        public string ClientId { get; set; }
        public string Channel { get; set; }
    }
}
