﻿using PoruciSaDaljine.Extensions;
using PoruciSaDaljine.Models;
using System.Text.Json;
using System.Threading.Tasks;

namespace PoruciSaDaljine
{
    public sealed partial class ChatClient
    {
        private async Task HandleMessageAsync(WSMessage message)
        {
            switch (message.CommandKey)
            {
                case WSMessage.COMMAND_KEY.CLIENT_SUBSCRIBE:
                    ControlMessage subscribeMessage = JsonSerializer.Deserialize<ControlMessage>(message.Payload);
                    await this.HandleSubscribeAsync(subscribeMessage);
                    break;
                case WSMessage.COMMAND_KEY.CLIENT_UNSUBSCRIBE:
                    ControlMessage unsubscribeMessage = JsonSerializer.Deserialize<ControlMessage>(message.Payload);
                    await this.HandleUnsubscribeAsync(unsubscribeMessage);
                    break;
                case WSMessage.COMMAND_KEY.CLIENT_MESSAGE:
                    ChatMessage chatMessage = JsonSerializer.Deserialize<ChatMessage>(message.Payload);
                    await this.HandleMessageAsync(chatMessage);
                    break;
                case WSMessage.COMMAND_KEY.CLIENT_GET_CHANNELS:
                    await this.HandleGetChannelsAsync(message);
                    break;
            }
        }

        private async Task HandleSubscribeAsync(ControlMessage subscribeMessage)
        {
            WSMessage outboundMessage = null;
            if (subscribeMessage.ClientId != this._state.ClientId && this._state.ClientId != null)
            {
                outboundMessage = new WSMessage
                {
                    CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                    Payload = "Greska: Id klijenta je pogresan."
                };
                outboundQueue.Add(outboundMessage.ToJson());
                return;
            }
            if (await _state.redisDB.HashExistsAsync(this._state.ClientId = subscribeMessage.ClientId, subscribeMessage.Channel))
            {
                outboundMessage = new WSMessage
                {
                    CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                    Payload = $"Greska: Korisnik je vec pretplacen na kanal: {subscribeMessage.Channel}"
                };
                outboundQueue.Add(outboundMessage.ToJson());
                return;
            }
            await this._state.subscriber.SubscribeAsync(subscribeMessage.Channel, this.OnRedisMessageHandler);
            await _state.redisDB.HashSetAsync(subscribeMessage.ClientId, subscribeMessage.Channel, "set");
            outboundMessage = new WSMessage
            {
                CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                Payload = $"Uspesno ste odabrali: {subscribeMessage.Channel}. Javite nam Vasu porudzbinu"
            };
            outboundQueue.Add(outboundMessage.ToJson());
        }

        private async Task HandleUnsubscribeAsync(ControlMessage unsubscribeMessage)
        {
            WSMessage outboundMessage = null;
            bool deleted = await _state.redisDB.HashDeleteAsync(this._state.ClientId, unsubscribeMessage.Channel);
            if (!deleted)
            {
                outboundMessage = new WSMessage
                {
                    CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                    Payload = $"Odjava sa kanala je neuspesna"
                };
                outboundQueue.Add(outboundMessage.ToJson());
                return;
            }
            await this._state.subscriber.UnsubscribeAsync(unsubscribeMessage.Channel, this.OnRedisMessageHandler);
            outboundMessage = new WSMessage
            {
                CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                Payload = $"Odjava sa kanala je uspesna"
            };
            outboundQueue.Add(outboundMessage.ToJson());
        }

        private async Task HandleMessageAsync(ChatMessage chatMessage)
        {
            if (!await this._state.redisDB.HashExistsAsync(chatMessage.ClientId, chatMessage.Channel))
            {
                WSMessage outboundMessage = new WSMessage
                {
                    CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                    Payload = $"Poruka se ne moze poslati. Klijent: {chatMessage.ClientId} ne postoji ili se nije pretplatio na kanal: {chatMessage.Channel}"
                };
                outboundQueue.Add(outboundMessage.ToJson());
            }
            await this._state.subscriber.PublishAsync(chatMessage.Channel, $"Kanal:{chatMessage.Channel}, Posiljalac:{chatMessage.ClientId}, Poruka:{chatMessage.Message}");
        }

        private async Task HandleGetChannelsAsync(WSMessage message)
        {
            var channels = await this._state.redisDB.HashGetAllAsync(this._state.ClientId);
            outboundQueue.Add(new WSMessage
            {
                CommandKey = WSMessage.COMMAND_KEY.SERVER_RESULT,
                Payload = channels.ToJson()
            }
            .ToJson());
        }
    }
}
