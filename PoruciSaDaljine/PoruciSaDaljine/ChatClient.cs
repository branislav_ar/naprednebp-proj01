﻿using PoruciSaDaljine.Extensions;
using PoruciSaDaljine.Models;
using StackExchange.Redis;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace PoruciSaDaljine
{
    public sealed partial class ChatClient
    {
        private const int BUFFER_SIZE = 1024;
        private State _state = new State();
        private BlockingCollection<string> outboundQueue = new BlockingCollection<string>();

        private Action<RedisChannel, RedisValue> onRedisMessageHandler = null;
        public Action<RedisChannel, RedisValue> OnRedisMessageHandler
        {
            get
            {
                if (this.onRedisMessageHandler == null)
                {
                    this.onRedisMessageHandler = new Action<RedisChannel, RedisValue>((channel, value) => this.outboundQueue.Add(value));
                }
                return this.onRedisMessageHandler;
            }
        }

        public ChatClient(ConnectionMultiplexer mux)
        {
            this._state.subscriber = mux.GetSubscriber();
            this._state.redisDB = mux.GetDatabase();
        }

        public async Task OutboundLoopAsync(WebSocket socket)
        {
            foreach(var item in this.outboundQueue.GetConsumingEnumerable())
            {
                var bytes = Encoding.UTF8.GetBytes(item);
                await socket.SendAsync(bytes, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }

        private async Task InboundLoopAsync(WebSocket socket)
        {
            byte[] inboundBuffer = ArrayPool<byte>.Shared.Rent(BUFFER_SIZE);
            try
            {
                while(true)
                {
                    WebSocketReceiveResult wsResult = await socket.ReceiveAsync(inboundBuffer, CancellationToken.None);
                    if(wsResult.MessageType == WebSocketMessageType.Close)
                    {
                        ArrayPool<byte>.Shared.Return(inboundBuffer);
                        return;
                    }
                    byte[] incomingBytes = inboundBuffer[0..wsResult.Count];
                    WSMessage _message = JsonSerializer.Deserialize<WSMessage>(Encoding.UTF8.GetString(incomingBytes));
                    await this.HandleMessageAsync(_message);
                }
            }
            finally
            {
                await this.CleanupSessionAsync();
            }
        }

        public async Task RunAsync(WebSocket socket)
        {
            this._state.outboundTaks = Task.Run(async () => await OutboundLoopAsync(socket));
            await this.InboundLoopAsync(socket);
        }


        private async Task CleanupSessionAsync()
        {
            foreach (var channelHash in await this._state.redisDB.HashGetAllAsync(this._state.ClientId))
            {
                await this._state.subscriber.UnsubscribeAsync(channelHash.Name.ToString(), this.OnRedisMessageHandler);
            }
            await this._state.redisDB.KeyDeleteAsync(this._state.ClientId);
        }

        
    }
}
