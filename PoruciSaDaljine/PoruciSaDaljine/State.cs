﻿using StackExchange.Redis;
using System.Threading.Tasks;

namespace PoruciSaDaljine
{
    internal class State
    {
        public string ClientId { get; set; }

        public Task outboundTaks;

        public ISubscriber subscriber;
        public IDatabase redisDB;
    }
}
