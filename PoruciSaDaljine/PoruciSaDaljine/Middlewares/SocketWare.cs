﻿using Microsoft.AspNetCore.Http;
using StackExchange.Redis;
using System.Threading.Tasks;

namespace PoruciSaDaljine.Middlewares
{
    public class SocketWare
    {
        private RequestDelegate next;
        private ConnectionMultiplexer mux;

        public SocketWare(RequestDelegate next, ConnectionMultiplexer mux)
        {
            this.next = next;
            this.mux = mux;
        }

        public async Task Invoke(HttpContext context)
        {
            if(!context.WebSockets.IsWebSocketRequest)
            {
                return;
            }
            using (var socket = await context.WebSockets.AcceptWebSocketAsync())
            {
                ChatClient client = new ChatClient(this.mux);
                await client.RunAsync(socket);
            }
        }

    }
}
