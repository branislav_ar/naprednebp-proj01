﻿using System.Text.Json;

namespace PoruciSaDaljine.Extensions
{
    public static class EncodingExtensions
    {
        public static string ToJson<T>(this T obj)
        {
            var data = JsonSerializer.Serialize(obj);
            return data;
        }
    }
}
